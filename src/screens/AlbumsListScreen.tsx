import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from 'react-native'
import AlbumCard from '../components/AlbumCard'
import axios from 'axios';

const AlbumsListScreen = ({route, navigation}) => {
    
    
    const  {data}  = route.params;
    const [albums, setAlbums] = useState([])

    useEffect(() => {
     getAlbums(data.id_artist)
    }, []);

    const getAlbums = (artists) =>{
      axios.get(`https://api.happi.dev/v1/music/artists/${artists}/albums?apikey=8eed63iA8aC7lq3SHHS8EmgqSOvVJWfncXjNPr6uRvH89dsoGYvkUqk7`)
      .then((response) => {
      const Data = response.data.result.albums
      console.log('Albums Data ::: ',Data)
      setAlbums(Data)
      }) 
    .catch((error) => { 
    console.log('ERRORss ::: ',error)
    }) 
    }

    return (
        <View style={styles.containar}>
         {albums?         
            <FlatList
           data={albums}
           renderItem={({ item }) => (
            <AlbumCard data={item}/>
            )}
            keyExtractor={item => item.id_album}/> : null}


           
        </View>
    )
}

export default AlbumsListScreen


const styles = StyleSheet.create({

 containar: {
     flex:1,
     backgroundColor: '#F5F4EB',
 },


});