import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native'
import Header from '../components/Header'
import Card from '../components/Card'
import Modal from '../components/Modal'
import axios from 'axios';
import {MusicApi, SearchApi, LyricApi, ArtistsApi} from '../config/api'

const LatestSongScreen = ({navigation}) => {

    const [trackData, settrackData] = useState('')
    const [imgItem, setImgItem] = useState('')
    const [openModal, setOpenModal] = useState(false)
    const [loading, setLoading] = useState(true)

    useEffect(() => {
     getLatestTrack()
     console.log(MusicApi)
    }, []);

    const getLatestTrack = () =>{
      axios.get(`https://api.happi.dev/v1/music/bpm/playlist/80-90?apikey=8eed63iA8aC7lq3SHHS8EmgqSOvVJWfncXjNPr6uRvH89dsoGYvkUqk7&limit=50`)
      .then((response) => {
      const Data = response.data.tracks
      settrackData(Data)
      setLoading(false)
      }) 
    .catch((error) => { 
    console.log('RESPONSE TRACK ::: ',error)
    }) 
    }

    const onImgTap =(item)=>{
        setImgItem(item.cover)
        setOpenModal(!openModal)
    }

    const onNameTap =(item)=>{
        if(item.haslyrics){
        console.log('NAVIGATE TO MusicLyric')
        navigation.navigate('MusicLyric',{data: item})
        }else{
            alert('Sorry this music dont have Lyric')
        }
        
    }

    const closeModal =()=>{
        setOpenModal(!openModal)
    }

    return (
        <View style={styles.containar}>
         {openModal ? <Modal img={imgItem} closeModal={()=> closeModal()}/> : null}
         <Header withH={1} handleUpdate={()=> getLatestTrack()}/>
         <View style={{padding: 10,}}>
         {loading ? <Text>Loading...</Text> :
         <View style={{height:'95%'}}>
         <FlatList
          keyExtractor={item => item.id_track}
          data={trackData}
          renderItem={({ item }) => (
          <Card
           data={item}
           onImgTap={()=> onImgTap(item)}
           onNameTap={()=> onNameTap(item)}/>
         )}/>
        </View>}
         </View>
        </View>
    )
}

export default LatestSongScreen

const styles = StyleSheet.create({

 containar: {
     flex:1,
     backgroundColor: '#F5F4EB',
 },
 title:{
     alignSelf: 'center',
     fontWeight: 'bold',
     fontSize:16,
 },
 image:{
     borderRadius:15,
     width: 65,
     height: 65,
 },
 lyricMode:{
     alignSelf: 'center',
     fontSize:12,
     padding: 3,
 },

});