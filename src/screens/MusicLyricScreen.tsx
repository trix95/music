import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native'
import Header from '../components/Header'
import axios from 'axios';

const MusicLyricScreen = ({route, navigation}) => {
    
    
    const { data } = route.params;
    const [lyrics, setLyrics] = useState('')

    useEffect(() => {
     getLyrics(data.id_artist, data.id_album, data.id_track)
    }, []);

    const getLyrics = (artists, albums, tracks) =>{
      axios.get(`https://api.happi.dev/v1/music/artists/${artists}/albums/${albums}/tracks/${tracks}/lyrics?apikey=8eed63iA8aC7lq3SHHS8EmgqSOvVJWfncXjNPr6uRvH89dsoGYvkUqk7&limit=50`)
      .then((response) => {
      const Data = response.data.result.lyrics
      console.log('Lyrics Data ::: ',Data)
      setLyrics(Data)
      }) 
    .catch((error) => { 
    console.log('ERROR ::: ',error)
    }) 
    }

    return (
        <View style={styles.containar}>
        <Header/>
        <View style={styles.contant}>
        <ScrollView>
            <Text style={styles.lyricsText}>{lyrics}</Text>
        </ScrollView>
        </View>
        </View>
    )
}

export default MusicLyricScreen


const styles = StyleSheet.create({

 containar: {
     flex:1,
     backgroundColor: '#F5F4EB',
 },
 contant: {
     backgroundColor: '#fff',
     height: '85%',
     padding: 15,
     margin:22,
     borderRadius:15
 },
 lyricsText: {
     fontSize:18,
     padding: 10,
     fontWeight: 'bold'
 }

});