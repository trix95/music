import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from 'react-native'
import ArtistCard from '../components/ArtistsCard'
import axios from 'axios';

const LatestSongScreen = ({navigation}) => {

    const [artist, setArtists] = useState('')

    useEffect(() => {
     getArtists()
    }, []);

    const getArtists = () =>{
      axios.get(`https://api.happi.dev/v1/music/artists?page=1&apikey=8eed63iA8aC7lq3SHHS8EmgqSOvVJWfncXjNPr6uRvH89dsoGYvkUqk7`)
      .then((response) => {
      const Data = response.data.result
        console.log('DATA ::: ',Data)
        setArtists(Data)
      }) 
    .catch((error) => { 
    console.log('ERROR ::: ',error)
    }) 
    }

    const moveToAlbums =(item)=>{
        navigation.navigate('AlbumList',{data: item})
    }

    return (
        <View style={styles.containar}>
        <FlatList
           data={artist}
           renderItem={({ item }) => (
            <TouchableOpacity onPress={()=> moveToAlbums(item)}>
            <ArtistCard data={item}/>
            </TouchableOpacity>
            )}
            keyExtractor={item => item.id_artist}/>
        </View>
    )
}

export default LatestSongScreen

const styles = StyleSheet.create({

 containar: {
     flex:1,
     backgroundColor: '#F5F4EB',
     padding: 10,
 },

});