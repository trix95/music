import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity,FlatList, TextInput } from 'react-native'
import axios from 'axios';
import SearchCard from '../components/SearchCard'

const SearchScreen = () => {

    const [qText, setQText] = useState('')
    const [resultSearhc, setResultSearhc] = useState('')

    const handleSearch = () =>{
      axios.get(`https://api.happi.dev/v1/music?q=${qText}&limit=50&apikey=8eed63iA8aC7lq3SHHS8EmgqSOvVJWfncXjNPr6uRvH89dsoGYvkUqk7&type=&lyrics=0`)
      .then((response) => {
      const Data = response.data.result
      console.log('RESULT SEARCH ::: ',Data)
      setResultSearhc(Data)
      }) 
    .catch((error) => { 
     console.log('ERROR ::: ',error)
     }) 
    }
    return (
        <View style={styles.containar}>
         {resultSearhc ? null : <View style={styles.top}/>}
           <View style={styles.searchBox}>

           <View style={styles.textBox}>
            <TextInput
             style={styles.inputText}
             onChangeText={setQText}
             value={qText}
             secureTextEntry={false} 
            />   
           </View>

           <TouchableOpacity onPress={()=> handleSearch()} style={styles.searchButton}>
            <Text style={styles.titleButton}>SEARCH</Text>
           </TouchableOpacity>
           </View>
           
          <View style={styles.contant}>
           <SearchCard data={resultSearhc}/>
           </View>
        </View>
    )
}

export default SearchScreen

const styles = StyleSheet.create({

 containar: {
     flex:1,
     backgroundColor: '#F5F4EB',
     padding: 10,
 },
 top: {
     height: '40%',
 },
 searchBox: {
     alignSelf: 'center',
     margin: 20,
     height: 70,
     width: '95%',
     backgroundColor: '#fff',
     borderRadius: 20,
     flexDirection: 'row',
 },
 textBox: {
  flex: 0.8,
  backgroundColor: '#fff',
  borderBottomLeftRadius:22,
  borderTopLeftRadius:22,
  alignItems: 'center',
  justifyContent: 'center',
 },
 searchButton: {
  flex: 0.2,
  backgroundColor:'black',
  borderBottomRightRadius: 22,
  borderTopRightRadius: 22,
  alignItems: 'center',
  justifyContent: 'center',
 },
 titleButton: {
     color: '#fff'
 },
 contant: {
     width: '100%'
 },
 inputText: {
     width: '100%',
     padding: 16,
 }

});