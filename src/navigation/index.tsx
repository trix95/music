/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';
import { ColorSchemeName, Pressable, Image } from 'react-native';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';

import ModalScreen from '../screens/ModalScreen';

import LatestSong from '../screens/LatestSongScreen'; 
import MusicLyric from '../screens/MusicLyricScreen';

import NotFoundScreen from '../screens/NotFoundScreen'

import ArtistsList from '../screens/ArtistsListScreen';
import AlbumList from '../screens/AlbumListScreen';


import AlbumsList from '../screens/AlbumsListScreen';
import SearchScreen from '../screens/SearchScreen';

import { RootStackParamList, RootTabParamList, RootTabScreenProps } from '../types';
import LinkingConfiguration from './LinkingConfiguration';

const Navigation = () => {
 

  return (
    <NavigationContainer>
          <RootNavigator />
    </NavigationContainer>
  );
}

/**
 * A root stack navigator is often used for displaying modals on top of all other content.
 * https://reactnavigation.org/docs/modal
 */
const Stack = createNativeStackNavigator<RootStackParamList>();

function RootNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Root" component={BottomTabNavigator} options={{ headerShown: false }} />
      <Stack.Screen name="NotFound" component={NotFoundScreen} options={{ title: 'Oops!' }} />
      <Stack.Group screenOptions={{ presentation: 'modal' }}>
        <Stack.Screen name="Modal" component={ModalScreen} />
      </Stack.Group>
    </Stack.Navigator>
  );
}


/**
 * A bottom tab navigator displays tab buttons on the bottom of the display to switch screens.
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */


 const LatestSongStack = createNativeStackNavigator();
const LatestSongStackScreen = () => (
  <LatestSongStack.Navigator>
    <LatestSongStack.Screen
      name="LatestSong"
      component={LatestSong}
      options={{headerShown: false}}
    />
    <LatestSongStack.Screen 
      name="MusicLyric"
      component={MusicLyric}
      options={{headerShown: true,
      shadowColor: 'transparent',
       title: 'Music Lyric ' ,
           headerStyle: {

            },
            }}
    />
  </LatestSongStack.Navigator>
);

 const ListArtistsStack = createNativeStackNavigator();
const ListArtistsStackScreen = () => (
  <ListArtistsStack.Navigator>
    <ListArtistsStack.Screen
      name="ArtistsList"
      component={ArtistsList}
      options={{title: 'Artists List',
      headerStyle: { elevation: 0, shadowOpacity: 0, borderBottomWidth: 0, }

}}
    />
    <ListArtistsStack.Screen
      name="AlbumList"
      component={AlbumsList}AlbumsList
      
      options={{title: 'Album List',
      headerStyle: { elevation: 0, shadowOpacity: 0, borderBottomWidth: 0, }

}}
    />
  </ListArtistsStack.Navigator>
);

const BottomTab = createBottomTabNavigator<RootTabParamList>();

function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="TabOne"
      screenOptions={{
        tabBarActiveTintColor: Colors[colorScheme].tint,
      }}>
      <BottomTab.Screen
        name="TabMusic"
        component={LatestSongStackScreen}
        options={{
           headerStyle: { elevation: 0, shadowOpacity: 0, borderBottomWidth: 0, },
          headerShown:false, 
          title: 'Music',
          tabBarIcon: ({ color }) => <TabBarIcon name="https://image.freepik.com/free-icon/black-music-icon_318-9277.jpg" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Artists"
        component={ListArtistsStackScreen}
        options={{
           headerStyle: { elevation: 0, shadowOpacity: 0, borderBottomWidth: 0, },
           headerShown:false,
          title: 'Artists',
          tabBarIcon: ({ color }) => <TabBarIcon name="http://clipart-library.com/new_gallery/2-21724_musical-music-artist-icon.png" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="AlbumsList"
        component={AlbumList}
        options={{
           headerStyle: { elevation: 0, shadowOpacity: 0, borderBottomWidth: 0, },
          title: 'Album',
          tabBarIcon: ({ color }) => <TabBarIcon name="https://cdn-icons-png.flaticon.com/512/26/26789.png" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="SearchScreen"
        component={SearchScreen}
        options={{
           headerStyle: { elevation: 0, shadowOpacity: 0, borderBottomWidth: 0, },
          title: 'Search Hear',
          tabBarIcon: ({ color }) => <TabBarIcon name="https://cdn3.iconfinder.com/data/icons/miniglyphs/500/014-512.png" color={color} />,
        }}
      />
    </BottomTab.Navigator>
  );
}

/**
 * You can explore the built-in icon families and icons on the web at https://icons.expo.fyi/
 */
function TabBarIcon(props: {
  
  name: React.ComponentProps<typeof FontAwesome>['name'];
  color: string;
}) {
  return <Image style={{width: 25, height:25}} source={{uri: props.name}} />;
}






export default () => {
  return (

      <Navigation />

  );
};

