import React,{useState} from 'react'
import { View, Text, Modal, StyleSheet, TouchableOpacity, Image, ScrollView } from 'react-native'

const InfoModal = ({item,closeModal}) => {

    const [modalVisible, setModalVisible] = useState(true);

    return (
    <View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>

         <TouchableOpacity onPress={closeModal}>
         <Text style={styles.close}>X</Text>
         </TouchableOpacity>

         <View style={styles.imageBox}>
          <Image
           style={styles.image}
           source={{
           uri: item.cover,
           }}
          />
         </View>

         <ScrollView style={styles.dataBox}>
          <Text style={styles.title}>Artist: {item.artist}.</Text>
          <Text style={styles.title}>Track: {item.track}.</Text>
          <Text style={styles.title}>Album: {item.album}.</Text>
         </ScrollView>

        </View>
      </Modal>
    </View>
    )
}

export default InfoModal


const styles = StyleSheet.create({
  
  centeredView: {
      padding: 15,
    alignSelf: 'center',
    height: '60%',
    width: '80%',
    backgroundColor: '#343434',
    marginTop: '40%',
    borderRadius: 10
  },
  close: {
    fontSize: 25,
    color: '#fff',
  },
  imageBox: {
      alignItems: 'center',
  },
  image: {
      width: 110,
      height: 110,
      borderRadius: 15,
      borderWidth:2,
      borderColor: '#fff'
  },
    title: {
      padding:10,
      fontSize: 20,
      fontWeight: 'bold',
      color: '#fff'
  },
  dataBox: {
      marginTop: 20,
  }


});
