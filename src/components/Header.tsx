import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

const Header = ({withH, title, handleUpdate}) => {
    return (
      <View style={{ width: '100%',height: withH?'11%' : '8%',justifyContent:'flex-end',backgroundColor: '#F5F4EB',}}>
       
        <View style={{flexDirection: 'row', alignItems: "center",}}>
        {//this (if) couse ''Music''
        title ?<Text style={styles.titlelyrics}>{title} Music</Text>
        :
        <Text style={styles.title}>Latist Track</Text>
        }

        {withH ?
          <TouchableOpacity onPress={handleUpdate} style={{flex: 0.2, width: 44, height: 44, justifyContent: 'center', alignItems: 'center',}}>
         <Text style={{color: 'red', fontWeight: 'bold'}}>Update</Text>
        </TouchableOpacity> : null}

        </View>

      </View>
    )
}
        // <View style={{width: 44, height: 44, backgroundColor: 'red',}}>
        // </View>

export default Header



const styles = StyleSheet.create({

  title: {
    flex: 0.8,
    marginLeft: 15,
    fontSize:20,
    fontWeight: 'bold'
    },
  titlelyrics: {
    flex: 0.8,
    marginLeft: 15,
    fontSize:20,
    fontWeight: 'bold',
    marginBottom: 15,
    },

});






      // <View style={{ width: '100%',height: withH?'11%' : '8%',justifyContent:'flex-end',backgroundColor: '#F5F4EB',}}>
      //   <View style={{flexDirection: 'column',}}>
      //   {//this (if) couse ''Music''
      //   title ?<Text style={styles.titlelyrics}>{title} Music</Text>
      //   :
      //   <Text style={styles.title}>Latist Track</Text>
      //   }

      //   <View style={{width: 44, height: 44, backgroundColor: 'red',}}>
      //   </View>

      //   </View>

      // </View>