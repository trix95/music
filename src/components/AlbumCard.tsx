import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'

const AlbumCard = ({data}) => {
    return (
        <View style={styles.card}>
          <Image
           style={styles.image}
           source={{
           uri: data.cover,
           }}/>

           <View style={styles.infoBox}>
            <Text>{data.album}</Text>           
            <Text>.......</Text>           
           </View>

         </View>
    )
}

export default AlbumCard


const styles = StyleSheet.create({

 card: {
     flex:1,
     width: '95%',
     height: 200,
     alignSelf: 'center',
     backgroundColor: '#fff',
     margin: 20,
     borderRadius:25,
     borderWidth: 0.5,
     flexDirection: 'row',
 },
   image: {
      width: 150,
      height: 198.8,
      borderBottomLeftRadius:25,
      borderTopLeftRadius:25, 
  },
  infoBox: {
      padding: 15,
      height: '100%',
  }


});