import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'

const ArtistCard = ({data}) => {
    return ( 
        <View style={styles.card}>
        <View style={{flex: 0.2}}>
        <Image
         style={styles.image}
         source={{
         uri: data.cover,
         }}/>
        </View>
        <View style={{flex: 0.6}}>
        <Text style={styles.title}>{data.artist}</Text>
        </View>
        </View>
    )
}

export default ArtistCard

const styles = StyleSheet.create({

 card: {
     padding: 2,
     flexDirection: 'row',
     alignSelf: 'center',
     margin: 10,
     alignItems: 'center',
     width: '100%',
     height: 80,
     backgroundColor: '#fff',
     borderRadius: 15,
     elevation:4
 },
 title:{
     alignSelf: 'center',
     fontWeight: 'bold',
     fontSize:16,
 },
 image:{
     borderRadius:100,
     width: 75,
     height: 75,
 },


});
