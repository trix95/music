import React from 'react'
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity } from 'react-native'

const MusicCard = ({data, onImgTap, onNameTap}) => {

    return (
     <View style={styles.card}>
        <TouchableOpacity onPress={onImgTap} style={{flex: 0.2}}>
        <Image
         style={styles.image}
         source={{
         uri: data.cover,
         }}
        />
        </TouchableOpacity>
        <TouchableOpacity onPress={onNameTap} style={{flex: 0.6}}>
        <Text style={styles.title}>{data.track}</Text>
        </TouchableOpacity>
        <View style={{flex: 0.2}}>
         {data.haslyrics ? //SMALL COMPONENT
         <View style={{flexDirection: 'row',justifyContent: 'center', alignItems: 'center',}}>
         <View style={{width:9, height:9, borderRadius:100, backgroundColor: 'green',}}/>
         <Text style={styles.lyricMode}>Lyrics</Text>
         </View>
         :
         <View style={{flexDirection: 'row',justifyContent: 'center', alignItems: 'center',}}>
         <View style={{width:9, height:9, borderRadius:100, backgroundColor: 'red',}}/>
         <Text style={styles.lyricMode}>Lyrics</Text>
         </View>}
        </View>
        </View>
    )
}

export default MusicCard

const styles = StyleSheet.create({

 card: {
     padding: 2,
     flexDirection: 'row',
     alignSelf: 'center',
     margin: 10,
     alignItems: 'center',
     width: '100%',
     height: 70,
     backgroundColor: '#fff',
     borderRadius: 15,
     elevation:3
 },
 title:{
     alignSelf: 'center',
     fontWeight: 'bold',
     fontSize:16,
 },
 image:{
     borderRadius:15,
     width: 65,
     height: 65,
 },
 lyricMode:{
     alignSelf: 'center',
     fontSize:12,
     padding: 3,
 },

});
