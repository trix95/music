import React,{useState} from 'react'
import { View, Text, StyleSheet, FlatList, Image,TouchableOpacity, Modal } from 'react-native'
import InfoModal from './InfoModal'

const SearchCard = ({data}) => {

    const [opneModa, setopenModa] = useState(false);
    const [info, setInfo] = useState(false);
    
    const handleModal =({item})=>{
      console.log('ggggggggggg', item)
       setInfo(item)
       setopenModa(!opneModa)
    }

    const closeModal =()=>{
      setopenModa(!opneModa)
    }

  const renderItem = ({ item }) => (
    <View style={styles.card}>
    {opneModa ? <InfoModal item={info} closeModal={()=> closeModal()}/> : null}
      <View style={styles.liftCard}>
        <Image
         style={styles.image}
         source={{
         uri: item.cover,
         }}
        />
      </View>

      <View style={styles.rightCard}>

       <View style={styles.titleBox}>
        <Text style={styles.title}>Track:</Text>
        <Text style={styles.titleResult}>{item.track.length > 17 ? item.track.slice(0, 18) : item.track}</Text>
        {item.track.length > 17 ? 
          <TouchableOpacity onPress={()=> handleModal({item})}>
          <Text style={styles.titleSeemore}>See more</Text>
          </TouchableOpacity>
          : null}
       </View>
       <View style={styles.titleBox}>
        <Text style={styles.title}>Artist:</Text>
        <Text style={styles.titleResult}>{item.artist.length > 17 ? item.artist.slice(0, 18) : item.artist}</Text>
        {item.artist.length > 17 ? 
          <TouchableOpacity  onPress={()=> handleModal({item})}>
          <Text style={styles.titleSeemore}>See more</Text>
          </TouchableOpacity>
          : null}
       </View>
       <View style={styles.titleBox}>
        <Text style={styles.title}>Album:</Text>
        <Text style={styles.titleResult}>{item.album.length > 17 ? item.album.slice(0, 18) : item.album}</Text>
        {item.album.length > 17 ? 
          <TouchableOpacity  onPress={()=> handleModal({item})}>
          <Text style={styles.titleSeemore}>See more</Text>
          </TouchableOpacity>
          : null}
       </View>

      </View>
    </View>
  );

    return (
        <View style={styles.list}>
         <FlatList
          data={data}
          renderItem={renderItem}
          keyExtractor={item => item.id_track}
          />
        </View>
    )
}

export default SearchCard

const styles = StyleSheet.create({
  
  list: {
    paddingTop: 12,
    paddingLeft: 12,
    paddingRight : 12,
    width:'100%',
    justifyContent: 'center',
  },
  card: {
    flexDirection: 'row',
    width: '100%',
    height: 140,
    borderBottomWidth:0.5,
    borderTopWidth:0.5,
    marginTop:20,
    justifyContent: 'center',
  },
  rightCard: {
    justifyContent: 'center',
    flex: 0.75,
  },
  liftCard: {
    flex: 0.25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleBox: {
      flexDirection: 'row',
      
  },
  title: {
      padding:5,
      fontSize: 17,
      fontWeight: 'bold'
  },
  titleResult: {
      padding:5,
      fontSize: 14,
  },
  titleSeemore: {
      fontSize: 11,
      marginTop: 13,
      color:'blue',
  },
  image: {
      width: 80,
      height: 80,
  },
});
