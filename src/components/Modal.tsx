import React,{useState} from 'react'
import { View, Text, Modal, TouchableOpacity, StyleSheet, ImageBackground } from 'react-native'

const Modals = ({ img, closeModal}) => {

    const [modalVisible, setModalVisible] = useState(true)

    return (
        <View style={styles.container}>
        <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
        >


        <View style={{flex: 1, backgroundColor: '#F5F4EB',}}>
          <ImageBackground source={{uri: img}} resizeMode="cover" style={styles.img}>
            <TouchableOpacity style={styles.close} onPress={closeModal}>
             <Text style={{fontSize: 20, fontWeight:'bold'}}> X </Text>
            </TouchableOpacity>
            </ImageBackground>
        </View>

        </Modal>
        </View>
    )
}

export default Modals


const styles = StyleSheet.create({
  
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F5F4EB',
    justifyContent: 'center',
  },
  
  close: {
    marginLeft:10,
    alignItems: 'center',
    width: 30,
    height: 30,
    borderRadius: 100,
    marginTop: '15%',
    backgroundColor: '#F5F4EB',
    justifyContent: 'center',
    borderWidth:1
  },
  img: {
    flex: 1,
  }


});
